(*remember first version*)
let remember =
	let mem = ref "" in
  	fun s -> 
  	let v = !mem in mem := s; v ;;


let gensym =
  let ctr = ref 0 in
  fun (s : string) ->
  	ctr := !ctr + 1;
  	s ^ string_of_int (!ctr - 1);;

    (* we wabt to access ctr and modify s before we change the value of ctr *)

let rec mappend (xs : 'a mlist) (ys : 'a mlist) : `a mlist = 
  match xs with
  | Nil -> ()
  | Cons(_h,t) ->
	   match !t with
		 | Nil -> t := ys 
		 | Cons(a,_) as m -> mappend m ys ;;



