(*remember solution*)
let remember1 =
  let mem = ref "" in
  fun (s : string) -> let v = !mem in mem := s; v ;;
(* gensym solution*)
let gensym =
  let ctr = ref 0 in
  fun (s : string) ->
    let v = s ^ string_of_int (!ctr) in
    ctr := !ctr + 1;
    v ;;

let rec mappend (xs : 'a mlist) (ys : 'a mlist) : unit = 
  match xs with
  | Nil -> ()
  | Cons(_h,t) -> match !t with
		 | Nil -> t := ys 
		 | Cons(_,_) as m -> mappend m ys ;;