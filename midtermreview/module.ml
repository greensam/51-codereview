module type MATHFUN = 
sig
	type t
	val times1 : t -> t
	val divideby1 : t -> t
	val add1 : t -> t
	val to_string : t -> string
end;;

module FunInt : MATHFUN = 
struct
	type t = int
	let times1 (x : t) : t = x * 1;;
	let divideby1 (x : t) : t = x / 1;;
	let add1 (x :t) : t = x + 1;;
	let to_string (x : t) : string = string_of_int x;;
end


module type OPERATORS =
sig
	type t
	val times : t -> t -> t
	val divide : t -> t -> t
	val one : t
	val plus : t -> t -> t
	val to_string : t -> string
end

module IntOps : OPERATORS =
struct
	type t = int
	let times = ( * )
	let divide = ( / )
	let plus = ( + )
	let one = 1
	let to_string = string_of_int
end

module FloatOps : OPERATORS with type t = float =
struct
	type t = float
	let times = ( *. )
	let divide = ( /. )
	let plus = ( +. )
	let one = 1.
	let to_string = string_of_float
end

module MakeMathFun (Ops : OPERATORS) : (MATHFUN with type t = Ops.t) = 
struct
	type t = Ops.t
	let times1 x = Ops.times (x) (Ops.one);; 
	let divideby1 x = Ops.divide (x) (Ops.one);;
	let add1 x = Ops.plus (x) (Ops.one);;
	let to_string = Ops.to_string
end

module IM = MakeMathFun(IntOps);;
module FM = MakeMathFun(FloatOps);;

