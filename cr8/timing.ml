(**********************************************************************
 * searchperf.ml: a performance test of search functionality that 
 *                outputs comma-separated values that report results.
 **********************************************************************)

open Crawl;;
open Crawler_services;;
module CS = Crawler_services;;

let read_file (fname : string) : string list =
  let chan = open_in fname in
  let rec aux (acc : string list) : string list =
    try
      aux ((input_line chan) :: acc)
    with
    | End_of_file -> close_in chan; List.rev acc in
  aux []
;;

(* run : execute a given number of search runs with the test queries *)
let run (n : int) (ndx : WordDict.dict) (words : string list) : unit =
  let len = List.length words in
  let gen_query (_ : unit) : string =
    "?q=" ^ 
      (let w = List.nth words (Random.int len) in
        (* 20% of the time, create a compound \query [AND or OR, even odds]*)
        if (Random.int 10) > 1 then
          let joiner = (if (Random.int 2) = 0 then " AND " else " OR ") in
          w ^ joiner ^ (List.nth words (Random.int len))
        else w) in
  let rec run' (c : int) : unit =
    if (c <= 0) then ()
    else 
      let _ = Q.eval_query ndx (Q.parse_query (gen_query ())) in
      run' (c - 1) in
  run' n
;;

let _ = Random.init 123456 in (* Always use the same seed *)
let index = (crawler ()) in
let words = (read_file "words.txt") in 
let _ =
  for _ = 1 to 1000 do
    let runstart = Unix.gettimeofday () in
    let _ = run 1000000 index words in
    let runend = Unix.gettimeofday () in
    let _ = (print_string ((string_of_float (runend -. runstart)))) in
    let _ = print_endline "" in
    let _ = flush_all () in ()
  done in 
()
;;