let rec to_run_length1 (lst : char list) : (int * char) list =
	 (* return a tuple of #repeitions and the remaining list ex. repeated chars *)
	 let rec remove_repeats (l : char list) : int * (char list) =
	   match l with
	   | [] -> (0,[])
	   | [_] -> (1,[])
	   | hd1::hd2::tl ->
	       if hd1 = hd2 then
	         let (num, remains) = remove_repeats (hd2::tl) in
	         (1 + num, remains)
	       else (1, hd2::tl) in
	 match lst with
	 | [] -> []
	 | hd::_ ->
	     let (num, remains) = remove_repeats lst in
	     (num, hd)::to_run_length1 remains;;
	

let rec to_run_length2 (x: char list) : (int * char) list =
 match x with
 | [] -> []
 | xhd::xtl ->
   match to_run_length2 xtl with
     | [] -> [(1, xhd)]
     | (n, c)::tl ->
       if xhd = c then
         (n + 1, c)::tl
       else
         (1, xhd)::(n, c)::tl
